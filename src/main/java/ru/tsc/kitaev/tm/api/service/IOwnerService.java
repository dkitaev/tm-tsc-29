package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    @Nullable
    E add(@Nullable String userId, @Nullable E entity);

    void remove(@Nullable String userId, @Nullable E entity);

    void clear(@Nullable String userId);

    @NotNull
    List<E> findAll(@Nullable String userId);

    @NotNull
    List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator);

    @Nullable
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    E removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    E removeByIndex(@Nullable String userId, @Nullable Integer index);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsByIndex(@Nullable String userId, int index);

    @NotNull
    Integer getSize(@Nullable String userId);

}
