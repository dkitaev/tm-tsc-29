package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.api.service.IOwnerService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    protected final IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> ownerRepository) {
        super(ownerRepository);
        this.ownerRepository = ownerRepository;
    }

    @Nullable
    @Override
    public E add(@Nullable final String userId, @Nullable final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new RuntimeException();
        return ownerRepository.add(userId, entity);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new RuntimeException();
        ownerRepository.remove(userId, entity);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        ownerRepository.clear(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    public E removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return ownerRepository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return ownerRepository.existsById(userId, id);
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, final int index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) return false;
        return ownerRepository.existsByIndex(userId, index);
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ownerRepository.getSize(userId);
    }

}
