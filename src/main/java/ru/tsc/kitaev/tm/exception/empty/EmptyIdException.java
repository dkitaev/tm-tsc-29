package ru.tsc.kitaev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty.");
    }

    public EmptyIdException(@NotNull String value) {
        super("Error" + value + " id is empty.");
    }

}
